# Ejercicios

## Consideraciones generales

### Ejercicio 1

Implemente la función moda, que reciba una lista de números enteros y devuelva la moda de la lista. En estadística, la moda es el valor que más se repite de una lista.

Tenga el resultado en Ejercicio1.py

### Ejercicio 2

Supongo que existe un sistema de plebiscitos (resultado SI o NO) con las siguientes reglas:

* El padrón electoral consiste de ´total´ personas (número entero)
* Hay dos opciones de voto: SI o NO. La cantidad de votos se representan con las variables votosSI y votosNO (números enteros). Notar que vostosSI + votosNO puede dar menos que total.
* El quorum corresponde al 40% del padrón electoral:
    * Si hay menos votos para el quorum, pero una de las opciones tiene una votación mayor o igual al 20% del padrón, gana dicha opción. Si no, no hay quorum y gana la opción NO.
	* Si hay suficientes votos para el quorum, gana la opción con más votos. En caso de empate, gana la opción NO
* Suponga que no hay votos blancos o nulos

Implemente una función que reciba los parámetros ´total´, ´votosSI´ y ´votosNO´, y devuelva un String con la opción ganadora ('SI, 'NO').

Tenga el resultado en Ejercicio2.py

### Ejercicio 3 
Implementa una función que recibe un string con el nombre de un archivo de texto y devuelve una lista que contiene aquellas palabras de largo N. Una palabra es cualquier texto (incluyendo símbolos) entre dos espacios en blanco. La lista debe  contener todas las palabras de largo N, incluyendo  sus repeticiones.

A modo de ejemplo, un algoritmo puede ser

* Abrir el archivo en modo lectura
* Leer el archivo y guardar todas las palabras en una lista
* Cerrar el archivo de texto
* Crear una lista vacía en donde se guardará el resultado final
* Recorrer la lista y obtener el largo de cada palabra, guardando en la lista aquellas palabras de largo N
* Retornar la lista con las palabras seleccionadas

Utilice el archivo de "texto.txt" adjunto en el repositorio.

Tenga el resultado en Ejercicio3.py
